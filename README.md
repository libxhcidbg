# Libxhcidbg - xHCI Debug Capability library

## Introduction

This **libxhcidbg** library implements support for the xHCI debug
capability as specified in [*eXtensible Host Controller Interface for
Universal Serial Bus (xHCI)*, revision 1.1, section 7.6][1]. Libxhcidbg
advertises the debug capability with vendor-id:device-id ffff:dbc1 and
device revision 00.01. Descriptor strings are set as follows:

    Product: HW.DbC
    Manufacturer: secunet
    SerialNumber: 1

## Usage with Linux host

The `usb-debug` driver of Linux can be used to interact with the xHCI
debug capability. To work with HW.DbC, one has to specify the
vendor-id:device-id tuple after driver loading:

    $ modprobe usb-debug
    $ echo ffff dbc1 >/sys/bus/usb-serial/drivers/debug/new_id

When the USB debugging cable is cold plugged, the host usually turns the
port into idle state and will not detect a remote debug capability
coming up. To prevent that, one has to disable automatic power
management. For an Intel xHCI host the following should do the trick:

    $ echo on | tee /sys/devices/pci0000\:00/0000\:00\:14.0/usb?/power/control

The debug capability should be visible as:
`/dev/serial/by-id/usb-secunet_HW.DbC_1-if00-port0`

For convenience, the `xhcidbc-log` shell script in the `misc/` directory
can be used to gather debug output from the device. It uses the
`inotifywait` utility (part of [*inotify-tools*][2]) to wait for the
device to be created and then display the output using `cat`. Either
execute the script as root or make sure the user has the necessary
access rights, e.g. is member of the `dialout` group.

If there is no visible console output despite the serial device being present,
the tty setting may be incorrect. Issue the following command to adjust the
parameters:

    $ stty -F /dev/serial/by-id/usb-secunet_HW.DbC_1-if00-port0 raw -echo

`stty` is part of the [*coreutils* software collection][3].

## BIOS considerations

On hardware platforms that provide USB port-switching, e.g. Intel Haswell
or Broadwell, the BIOS option *USB 3.0 Mode* **must** be set to
*Enabled*. Otherwise the USB ports may not be routed to the xHCI
controller and thus no connected device will be recognized during
initialization.

If your BIOS has a "USB charging mode" configuration option, it is recommended
to turn this feature off.

## ModemManager considerations

The ModemManager service may interact with the USB serial device created by the
debugging capability. To avoid any interference, the service should be stopped:

    $ sudo systemctl stop ModemManager.service

[1]: https://www-ssl.intel.com/content/www/us/en/io/universal-serial-bus/extensible-host-controler-interface-usb-xhci.html
[2]: https://github.com/rvoicilas/inotify-tools/wiki
[3]: https://www.gnu.org/software/coreutils/
