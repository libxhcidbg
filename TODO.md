# General


## Documentation

Document the build process for a stand-alone library (utilizing
*libhwbase*'s build system).


## SPARK

* Prove absence of runtime errors


## Configuration options

There are various configuration options throughout the code that
should be moved into `HW.DbC_Config`.


# Current Code


## `Receive`

 * Do not unconditionally poll in Receive and respect poll
   deadline.


## `Transfers.Send`

* On `Start_Now`, make sure we start transferring even if we
  only appended to an already queued transfer. Or make it part
  of the contract that we are never called with `Start_Now =>
  True` when an already queued but not started transfer exists
  (which is currently true).

* Don't acquire a `Transfer_Id` (or release it on failure) if
  we can't enqueue it. Or make it part of the contract, that
  the `Transfer_Rings` are always big enough to hold all
  `Transfer_Id`s (which is currently true).


## `Transfer_Info`

* Use better strings in receive statistics

* Use wider numeric types for statistics


## `DMA_Buffers`

Currently, DMA addresses are precalculated which results in
redundancies with constants used throughout the code. We could
calculate DMA addresses based on compile time constants instead.
