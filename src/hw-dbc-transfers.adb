--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

with System;

with HW.Debug;
with HW.DbC.Transfer_Info;
with HW.DbC.Transfer_Rings;

package body HW.DbC.Transfers
is

   Debug_TDs : constant Boolean := False;

   Num_Receive_TDs : constant := 2;

   ----------------------------------------------------------------------------

   procedure Start_Bulk (Id : DbC.Transfer_Id; Start_Now : Boolean)
   is
      EP : constant Endpoint_Range  := Transfer_Info.Get_Endpoint (Id);
      Len : constant Natural        := Transfer_Info.Get_Length (Id);
      Pointer : constant Word64     := Transfer_Info.Physical (Id);
   begin
      -- It's just one TRB.
      Transfer_Rings.Enqueue_Data_TRB
        (EP                => EP,
         Data_Length       => Len,
         Data_Buf          => Pointer,
         Toggle_CS         => Start_Now);

      if Start_Now then
         Ring_Doorbell (EP);
      end if;

      pragma Debug (Debug_TDs, Debug.Put ("TD  started; Length: "));
      pragma Debug (Debug_TDs, Debug.Put_Int32 (Int32 (Len)));
      pragma Debug (Debug_TDs, Debug.Put ("; Transfer: "));
      pragma Debug (Debug_TDs, Debug.Put_Word16 (Word16 (Id)));
      pragma Debug (Debug_TDs, Debug.Put_Reg64 ("; Pointer", Pointer));
   end Start_Bulk;

   ----------------------------------------------------------------------------

   procedure Reset (Initial_Reset : Boolean)
   is
      Id : Transfer_Id;
      Ctr : Word64;
      Success : Boolean;
   begin
      Transfer_Rings.Initialize (2);
      Transfer_Rings.Initialize (3);

      if Initial_Reset then
         for I in 1 .. Num_Receive_TDs loop
            Transfer_Info.Start
              (Endpoint    => 3,
               Length      => Max_Bulk_Size,
               Id          => Id,
               Success     => Success);
            exit when not Success;
            Start_Bulk (Id, False);
         end loop;
      else
         Ctr := 0;
         loop
            Transfer_Info.Walk_Started (Ctr, Id, Success);
            exit when not Success;
            Start_Bulk (Id, False);
         end loop;
      end if;
   end Reset;

   procedure Start
   is
   begin
      for EP in Endpoint_Range loop
         Transfer_Rings.Toggle_CS (EP);
         Ring_Doorbell (EP);
      end loop;
   end Start;

   ----------------------------------------------------------------------------

   subtype DMA_Range is Natural range 0 .. Max_Bulk_Size - 1;
   subtype DMA_Buffer is Buffer (DMA_Range);

   procedure Copy_DMA_In
     (Id       : in     Transfer_Id;
      Buf      : in out Buffer;
      Len      : in     Natural;
      DMA_Off  : in     Natural);
   procedure Copy_DMA_In
     (Id       : in     Transfer_Id;
      Buf      : in out Buffer;
      Len      : in     Natural;
      DMA_Off  : in     Natural)
   with SPARK_Mode => Off
   is
      DMA_Buf : DMA_Buffer
      with Address => System'To_Address (Transfer_Info.Physical (Id));
      DMA_Len : constant Natural := Natural'Min (Max_Bulk_Size - DMA_Off, Len);
   begin
      Buf (Buf'First .. Buf'First + DMA_Len - 1) :=
         DMA_Buf (DMA_Off .. DMA_Off + DMA_Len - 1);
   end Copy_DMA_In;

   procedure Copy_DMA_Out
     (Id       : Transfer_Id;
      Buf      : Buffer;
      Off      : Natural;
      Len      : Natural;
      DMA_Off  : Natural := 0);
   procedure Copy_DMA_Out
     (Id       : Transfer_Id;
      Buf      : Buffer;
      Off      : Natural;
      Len      : Natural;
      DMA_Off  : Natural := 0)
   with SPARK_Mode => Off
   is
      DMA_Buf : DMA_Buffer
      with Address => System'To_Address (Transfer_Info.Physical (Id));
      DMA_Len : constant Natural := Natural'Min (Max_Bulk_Size - DMA_Off, Len);
   begin
      DMA_Buf (DMA_Off .. DMA_Off + DMA_Len - 1) :=
         Buf (Off .. Off + DMA_Len - 1);
   end Copy_DMA_Out;

   ----------------------------------------------------------------------------

   procedure Receive
     (Buf : in out Buffer;
      Len : in out Natural)
   is
      Id : Transfer_Id;
      Ctr : Word64;
      Success : Boolean;
   begin
      Ctr := 0;
      loop
         Transfer_Info.Walk_Finished (3, Ctr, Id, Success);
         exit when
            not Success or else
            (Transfer_Info.Get_Status (Id) = DbC.Success or
             Transfer_Info.Get_Status (Id) = DbC.Data_Residue);
         Transfer_Info.Restart (Id, Max_Bulk_Size);  -- ignore failed transfers
         Start_Bulk (Id, True);
      end loop;
      if Success then
         Len := Natural'Min
           (Len,
            Transfer_Info.Get_Length (Id) - Transfer_Info.Get_Offset (Id));
         Copy_DMA_In
           (Id       => Id,
            Buf      => Buf,
            Len      => Len,
            DMA_Off  => Transfer_Info.Get_Offset (Id));
         Transfer_Info.Set_Offset (Id, Transfer_Info.Get_Offset (Id) + Len);
         if Transfer_Info.Get_Offset (Id) = Transfer_Info.Get_Length (Id) then
            Transfer_Info.Restart (Id, Max_Bulk_Size);
            Start_Bulk (Id, True);
         end if;
      else
         Len := 0;
      end if;
   end Receive;

   ----------------------------------------------------------------------------

   procedure Process_Finished_Sends
   is
      Id : Transfer_Id;
      Ctr : Word64;
      Success : Boolean;
   begin
      Ctr := 0;
      loop
         Transfer_Info.Walk_Finished (2, Ctr, Id, Success);
         exit when not Success;
         Transfer_Info.Reset (Id);
      end loop;
   end Process_Finished_Sends;

   ----------------------------------------------------------------------------

   procedure Send
     (Buf         : in     Buffer;
      Len         : in out Natural;
      Start_Now   : in     Boolean;
      Success     :    out Boolean)
   is
      Id : Transfer_Id;
      Offset : Natural;
      Appended : Natural := 0;
   begin
      if not Transfer_Rings.Last_Started (2) then
         Appended := Len;
         Transfer_Info.Append (2, Appended, Offset, Id);
         if Appended > 0 then
            Copy_DMA_Out
              (Id       => Id,
               Buf      => Buf,
               Off      => Buf'First,
               Len      => Appended,
               DMA_Off  => Offset);
            Transfer_Rings.Requeue_Data_TRB
              (EP       => 2,
               Length   => Transfer_Info.Get_Length (Id),
               Buf_Addr => Transfer_Info.Physical (Id));
         end if;
      end if;
      Success := True;

      if Len > Appended then
         Len := Natural'Min (Len - Appended, Max_Bulk_Size);

         Process_Finished_Sends;

         Transfer_Info.Start
           (Endpoint    => 2,
            Length      => Len,
            Id          => Id,
            Success     => Success);

         if Success and not Transfer_Rings.Full (2) then
            Copy_DMA_Out
              (Id       => Id,
               Buf      => Buf,
               Off      => Buf'First + Appended,
               Len      => Len);

            Start_Bulk (Id, Start_Now);
         else
            Success := False;
         end if;

         if Success then
            Len := Len + Appended;
         else
            Len := Appended;
         end if;
      end if;
   end Send;

end HW.DbC.Transfers;

--  vim: set ts=8 sts=3 sw=3 et:
