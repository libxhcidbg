--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

with HW.Debug;
with HW.DbC.DMA_Buffers;

package body HW.DbC.Transfer_Info
with
   Refined_State => (State => (Start_Counter, Stats, Xfers))
is

   Start_Counter : Word64 := 0;  -- Should not overflow

   type Xfer_Info is record
      Endpoint : Endpoint_Range;
      Token    : Word64;
      Offset   : Natural;
      Length   : Natural;
      Status   : Error;
      Started  : Boolean;
      Finished : Boolean;
   end record;

   type Xfer_Array is array (Transfer_Id) of Xfer_Info;

   Xfers : Xfer_Array := Xfer_Array'
     (Transfer_Id => Xfer_Info'
        (Endpoint => Endpoint_Range'First,
         Token    => 16#0000_0000_0000_0000#,
         Offset   => 0,
         Length   => 0,
         Status   => Error'First,
         Started  => False,
         Finished => False));

   type Stats_Info is record
      Enqueued    : Word32;
      Transfered  : Word32;
      Lost        : Word32;
   end record;
   type Stats_Array is array (Endpoint_Range) of Stats_Info;
   Stats : Stats_Array := (Endpoint_Range => (others => 0));

   ----------------------------------------------------------------------------

   function Get_Endpoint (Id : Transfer_Id) return Endpoint_Range
   is
   begin
      return Xfers (Id).Endpoint;
   end Get_Endpoint;

   function Get_Offset (Id : Transfer_Id) return Natural
   is
   begin
      return Xfers (Id).Offset;
   end Get_Offset;

   procedure Set_Offset (Id : Transfer_Id; Offset : Natural)
   is
   begin
      Xfers (Id).Offset := Offset;
   end Set_Offset;

   function Get_Length (Id : Transfer_Id) return Natural
   is
   begin
      return Xfers (Id).Length;
   end Get_Length;

   function Get_Status (Id : Transfer_Id) return Error
   is
   begin
      return Xfers (Id).Status;
   end Get_Status;

   function Physical (Id : Transfer_Id) return Word64
   is
      use type Word64;
   begin
      return DMA_Buffers.Base + Word64 (Id) * Max_Bulk_Size;
   end Physical;

   ----------------------------------------------------------------------------

   procedure Start
     (Endpoint : in     Endpoint_Range;
      Length   : in     Natural;
      Id       :    out Transfer_Id;
      Success  :    out Boolean)
   is
      use type Word32;
      use type Word64;
   begin
      Success := False;
      Id := Transfer_Id'First;
      for I in Transfer_Id loop
         if not Xfers (I).Started then
            Xfers (I) :=
              (Endpoint => Endpoint,
               Token    => Start_Counter,
               Offset   => 0,
               Length   => Length,
               Status   => Error'First,
               Started  => True,
               Finished => False);
            Stats (Endpoint).Enqueued :=
              Stats (Endpoint).Enqueued + Word32 (Length);
            Start_Counter := Start_Counter + 1;
            Success := True;
            Id := I;
         end if;
         exit when Success;
      end loop;
   end Start;

   procedure Restart
     (Id       : Transfer_Id;
      Length   : Natural)
   is
      use type Word32;
      use type Word64;

      Endpoint : constant Endpoint_Range := Xfers (Id).Endpoint;
   begin
      Xfers (Id) :=
        (Endpoint => Endpoint,
         Token    => Start_Counter,
         Offset   => 0,
         Length   => Length,
         Status   => Error'First,
         Started  => True,
         Finished => False);
      Start_Counter := Start_Counter + 1;
      Stats (Endpoint).Enqueued
        := Stats (Endpoint).Enqueued + Word32 (Length);
   end Restart;

   procedure Append
     (Endpoint : in     Endpoint_Range;
      Length   : in out Natural;
      Offset   :    out Natural;
      Id       :    out Transfer_Id)
   is
      use type Word32;
      use type Word64;
      Success : Boolean;
      Max_Counter : Word64;
   begin
      Success := False;
      Max_Counter := 0;
      Id := Transfer_Id'First;
      for I in Transfer_Id loop
         if Xfers (I).Started and
            Xfers (I).Endpoint = Endpoint and
            Xfers (I).Token >= Max_Counter
         then
            Max_Counter := Xfers (I).Token;
            Success := True;
            Id := I;
         end if;
      end loop;
      if Success then
         Length := Natural'Min (Length, Max_Bulk_Size - Xfers (Id).Length);
         Offset := Xfers (Id).Length;
         Xfers (Id).Length := Xfers (Id).Length + Length;
         Stats (Endpoint).Enqueued
           := Stats (Endpoint).Enqueued + Word32 (Length);
      else
         Length := 0;
         Offset := 0;
      end if;
   end Append;

   procedure Finish
     (DMA_Physical      : Word64;
      Remaining_Length  : Natural;
      Status            : Error)
   is
      use type Word32;
      use type Word64;
      Id : Transfer_Id;
   begin
      if Physical (Transfer_Id'First) <= DMA_Physical and
         DMA_Physical <= Physical (Transfer_Id'Last) and
         (DMA_Physical - Physical (Transfer_Id'First)) mod Max_Bulk_Size = 0
      then
         Id := Transfer_Id
           ((DMA_Physical - Physical (Transfer_Id'First)) / Max_Bulk_Size);
         if Xfers (Id).Started then
            Xfers (Id).Finished := True;
            Xfers (Id).Status := Status;
            Xfers (Id).Offset := 0;
            if Remaining_Length <= Xfers (Id).Length then
               Xfers (Id).Length := Xfers (Id).Length - Remaining_Length;
               Stats (Xfers (Id).Endpoint).Transfered :=
                 Stats (Xfers (Id).Endpoint).Transfered + Word32
                   (Xfers (Id).Length);
               Stats (Xfers (Id).Endpoint).Lost :=
                 Stats (Xfers (Id).Endpoint).Lost + Word32 (Remaining_Length);
            else
               Stats (Xfers (Id).Endpoint).Lost :=
                 Stats (Xfers (Id).Endpoint).Lost + Word32 (Xfers (Id).Length);
               Xfers (Id).Length := 0;
               Xfers (Id).Status := Controller_Error;
            end if;
         else
            pragma Debug (Debug.Put_Reg8
              ("Spurious finish for transfer", Word8 (Id)));
            null;
         end if;
      else
         pragma Debug (Debug.Put_Reg64
           ("WARNING: Invalid DMA pointer", DMA_Physical));
         pragma Debug (Debug.Put_Reg64
           ("           first DMA address", Physical (Transfer_Id'First)));
         pragma Debug (Debug.Put_Reg64
           ("            last DMA address", Physical (Transfer_Id'Last)));
         null;
      end if;
   end Finish;

   procedure Reset (Id : Transfer_Id)
   is
   begin
      Xfers (Id).Started := False;
   end Reset;

   ----------------------------------------------------------------------------

   generic
      with function Check (Id : Transfer_Id) return Boolean;
   procedure Walk
     (Minimum_Ctr : in out Word64;
      Id          :    out Transfer_Id;
      Success     :    out Boolean);

   procedure Walk
     (Minimum_Ctr : in out Word64;
      Id          :    out Transfer_Id;
      Success     :    out Boolean)
   is
      use type Word64;
   begin
      Success := False;
      Id := Transfer_Id'First;
      for I in Transfer_Id loop
         if Check (I) and Xfers (I).Token >= Minimum_Ctr then
            if (Success and Xfers (I).Token < Xfers (Id).Token) or
               not Success
            then
               Id := I;
               Success := True;
            end if;
         end if;
      end loop;
      if Success then
         Minimum_Ctr := Xfers (Id).Token + 1;
      end if;
   end Walk;

   function Check_Started (Id : Transfer_Id) return Boolean
   is
   begin
      return Xfers (Id).Started and not Xfers (Id).Finished;
   end Check_Started;
   procedure Walk_Started_Inst is new Walk (Check_Started);
   procedure Walk_Started
     (Minimum_Ctr : in out Word64;
      Id          :    out Transfer_Id;
      Success     :    out Boolean)
      renames Walk_Started_Inst;

   procedure Walk_Finished
     (Endpoint    : in     Endpoint_Range;
      Minimum_Ctr : in out Word64;
      Id          :    out Transfer_Id;
      Success     :    out Boolean)
   is
      function Check_Finished (Id : Transfer_Id) return Boolean;
      function Check_Finished (Id : Transfer_Id) return Boolean
      is
      begin
         return
            Xfers (Id).Endpoint = Endpoint and
            Xfers (Id).Started and
            Xfers (Id).Finished;
      end Check_Finished;

      procedure Walk_Finished_Inst is new Walk (Check_Finished);
   begin
      Walk_Finished_Inst (Minimum_Ctr, Id, Success);
   end Walk_Finished;

   ----------------------------------------------------------------------------

   procedure Dump_Stats
   is
      use type Word32;
   begin
      Debug.Put_Line ("DbC statistics:");
      for EP in Endpoint_Range loop
         Debug.Put ("  Endpoint #");
         Debug.Put_Int8 (Int8 (EP));
         Debug.New_Line;
         Debug.Put ("      enqueued: ");
         Debug.Put_Word32 (Stats (EP).Enqueued);
         Debug.Put (" (");
         Debug.Put_Int32 (Int32 (Stats (EP).Enqueued));
         Debug.Put_Line (")");
         Debug.Put ("    transfered: ");
         Debug.Put_Word32 (Stats (EP).Transfered);
         Debug.Put (" (");
         Debug.Put_Int32 (Int32 (Stats (EP).Transfered));
         Debug.Put_Line (")");
         Debug.Put ("          lost: ");
         Debug.Put_Word32 (Stats (EP).Lost);
         Debug.Put (" (");
         Debug.Put_Int32 (Int32 (Stats (EP).Lost));
         Debug.Put_Line (")");
         Debug.Put ("      in queue: ");
         Debug.Put_Word32
           (Stats (EP).Enqueued - Stats (EP).Transfered - Stats (EP).Lost);
         Debug.Put (" (");
         Debug.Put_Int32 (Int32
           (Stats (EP).Enqueued - Stats (EP).Transfered - Stats (EP).Lost));
         Debug.Put_Line (")");
      end loop;
   end Dump_Stats;

end HW.DbC.Transfer_Info;

--  vim: set ts=8 sts=3 sw=3 et:
