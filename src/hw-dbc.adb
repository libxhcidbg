--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

with System;

with HW.Time;
with HW.Debug;

with HW.DbC.Intel_Quirk;
with HW.DbC.DMA_Buffers;
with HW.DbC.Transfer_Info;
with HW.DbC.Transfer_Rings;
with HW.DbC.Transfers;
with HW.DbC.Contexts;
with HW.DbC.Events;
with HW.DbC.TRBs;

package body HW.DbC
with
   Refined_State => (State => (Connected, Running,
                               DbC_Run_Deadline, DbC_Poll_Deadline,
                               DbC_Stat_Deadline, Events.State,
                               Transfer_Info.State, Transfer_Rings.State),
                     DMA   => (ERST, DbC_Context, Desc_Strings, Events.DMA,
                               Transfer_Rings.DMA))
is

   Apply_Intel_Quirk : constant Boolean := True;
   Debug_xCap        : constant Boolean := False;

   Connected         : Boolean := False;
   Running           : Boolean := False;
   DbC_Run_Deadline  : Time.T  := Time.T_First;
   DbC_Poll_Deadline : Time.T  := Time.T_First;
   DbC_Stat_Deadline : Time.T  := Time.T_First;

   ----------------------------------------------------------------------------

   ERST : Events.ERST_Entry
   with
      Address => System'To_Address (DMA_Buffers.Event_Ring_Segment_Table_Base);

   DbC_Context : Contexts.DbC_Context
   with
      Address => System'To_Address (DMA_Buffers.DbC_Context_Base);

   ----------------------------------------------------------------------------

   subtype Desc_Str_Range is Natural range 0 .. 14;
   type Desc_Str is array (Desc_Str_Range) of Word16 with Pack;
   type String_Descriptor is record
      bLength           : Byte;
      bDescriptor_Type  : Byte;
      wData             : Desc_Str;
   end record with Pack;

   Null_String_Desc : constant String_Descriptor
     := (bLength          => 0,
         bDescriptor_Type => 0,
         wData            => (others => 0));

   type Desc_Strings_Type is (String0, Manufacturer, Product, Serial_Number);
   type Desc_Strings_Array is
      array (Desc_Strings_Type) of String_Descriptor with Pack;
   Desc_Strings : Desc_Strings_Array := (others => Null_String_Desc)
   with
      Address => System'To_Address (DMA_Buffers.Descriptor_Strings_Base);

   procedure String_To_Desc (Dst : out String_Descriptor; Src : in String)
   is
      use type Byte;
   begin
      Dst.bLength := 2 + 2 * Byte'Min (Dst.wData'Length, Src'Length);
      Dst.bDescriptor_Type := 16#03#;
      for I in Desc_Str_Range loop
         if I < Src'Last then
            Dst.wData (I) := Character'Pos (Src (I + 1));
         else
            Dst.wData (I) := 16#0000#;
         end if;
      end loop;
   end String_To_Desc;

   ----------------------------------------------------------------------------

   procedure Find_Next_xCap (Cap_Id : in Word8; Success : out Boolean)
   is
      use type Word8;
      use type Word32;
      Current_Id : Word8;
      Temp_Offset : Word32;
   begin
      Success := False;
      if xCap_Regs.Byte_Offset = 0 then
         Cap_Regs.Read (Temp_Offset, XHCI_Extended_Caps);
      else
         xCap_Regs.Read (Temp_Offset, Next_xCap);
      end if;
      loop
         Temp_Offset := Shift_Left (Temp_Offset, 2);
         pragma Debug (Debug_xCap, Debug.Put_Reg32
           ("Find_Next_xCap Offset", Temp_Offset));
         exit when
            Temp_Offset = 0 or else
            xCap_Regs.Byte_Offset > MMIO_Size - Natural (Temp_Offset) - 2;

         xCap_Regs.Byte_Offset := xCap_Regs.Byte_Offset + Natural (Temp_Offset);

         xCap_Regs.Read (Current_Id, Capability_ID);
         Success := Current_Id = Cap_Id;
         pragma Debug (Debug_xCap, Debug.Put_Reg8
           ("Find_Next_xCap Cap_Id", Current_Id));
         exit when Success;

         xCap_Regs.Read (Temp_Offset, Next_xCap);
      end loop;
   end Find_Next_xCap;

   ----------------------------------------------------------------------------

   procedure BIOS_Handover (Success : out Boolean)
   is
      use type Word8;
      BIOS_Owned  : Word8;
      Deadline    : Time.T;
   begin
      xCap_Regs.Byte_Offset := 0;
      Find_Next_xCap (1, Success);
      if Success then
         Legacy_Support_Regs.Byte_Offset := xCap_Regs.Byte_Offset;
         -- See if the BIOS claims ownership
         Legacy_Support_Regs.Read (BIOS_Owned, HC_BIOS_Owned_Semaphore);
         if BIOS_Owned = 1 then
            pragma Debug (Debug.Put_Line ("DbC: BIOS claims ownership."));

            Legacy_Support_Regs.Write (HC_OS_Owned_Semaphore, Word8'(1));

            Deadline := Time.MS_From_Now (5_000);
            loop
               Legacy_Support_Regs.Read (BIOS_Owned, HC_BIOS_Owned_Semaphore);
               exit when BIOS_Owned = 0;
               declare
                  Timeout : constant Boolean := Time.Timed_Out (Deadline);
               begin
                  Success := not Timeout;
               end;
               exit when not Success;
               pragma Warnings (GNATprove, Off, "statement has no effect");
               for I in 0 .. 1234 loop
                  null; -- Busy loop to reduce pressure on HC BIOS Owned
                        -- Semaphore. It shouldn't generate an SMI but
                        -- might congest the xHC?
               end loop;
               pragma Warnings (GNATprove, On, "statement has no effect");
            end loop;

            pragma Debug (not Success, Debug.Put_Line
              ("ERROR: BIOS didn't hand over xHC within 5s."));
            pragma Debug (Success, Debug.Put_Line
              ("DbC: BIOS hand-over succeeded."));
         end if;
      end if;
   end BIOS_Handover;

   procedure Reset (Initial_Reset : Boolean := False);

   procedure Init
   is
      use type Word8;
      CNR : Word8;
      Deadline : Time.T;
      Success : Boolean;
      Cap_Length : Word8;
   begin
      Cap_Regs.Read (Cap_Length, Capability_Registers_Length);
      Op_Regs.Byte_Offset := Natural (Cap_Length);

      Op_Regs.Read (CNR, Controller_Not_Ready);
      Success := CNR = 0;

      if not Success then
         pragma Debug (Debug.Put_Line ("WARNING: xHCI not ready!"));
         Deadline := Time.MS_From_Now (1_000);
         Success := True;
         loop
            Op_Regs.Read (CNR, Controller_Not_Ready);
            exit when CNR = 0;
            declare
               Timed_Out : constant Boolean := Time.Timed_Out (Deadline);
            begin
               Success := not Timed_Out;
            end;
            exit when not Success;
         end loop;
         pragma Debug (not Success, Debug.Put_Line
           ("ERROR: xHC not ready after 1s."));
      end if;

      if Success then
         BIOS_Handover (Success);
      end if;

      if Success then
         xCap_Regs.Byte_Offset := 0;
         Find_Next_xCap (10, Success);
      end if;

      pragma Debug (not Success, Debug.Put_Line
                    ("ERROR: Couldn't find xHCI debug capability."));

      if Success then
         Regs.Byte_Offset := xCap_Regs.Byte_Offset;

         ERST := Events.ERST_Entry'
           (Segment_Base   => DMA_Buffers.Event_Ring_Base,
            Segment_Size   => TRBs.TRBs_Per_Ring,
            Reserved_Z     => 0);

         Desc_Strings (String0).bLength            := 16#04#;
         Desc_Strings (String0).bDescriptor_Type   := 16#03#;
         Desc_Strings (String0).wData := (0 => 16#0409#, others => 16#0000#);
         String_To_Desc (Desc_Strings (Manufacturer), "secunet");
         String_To_Desc (Desc_Strings (Product), "HW.DbC");
         String_To_Desc (Desc_Strings (Serial_Number), "1");

         Reset (Initial_Reset => True);
      end if;
   end Init;

   ----------------------------------------------------------------------------

   procedure Reset (Initial_Reset : Boolean := False)
   is
      use type Word8;
      use type Word16;
      use type Word64;
      DCE,
      MBS : Word8;
   begin
      if Regs.Byte_Offset /= 0 then
         Regs.Write (DbC_Enable, Word8'(0));
         loop
            Regs.Read (DCE, DbC_Enable);
            exit when DCE = 0;
         end loop;

         Transfers.Reset (Initial_Reset);

         Regs.Write (ERST_Size, Word16'(1));
         Regs.Write (ERST_Base_Lo, Word32
           (DMA_Buffers.Event_Ring_Segment_Table_Base mod 16#1_0000_0000#));
         Regs.Write (ERST_Base_Hi, Word32
           (DMA_Buffers.Event_Ring_Segment_Table_Base  /  16#1_0000_0000#));
         Events.Reset_Ring;

         Regs.Write (ER_Dequeue_Ptr_Lo, Word32
           (DMA_Buffers.Event_Ring_Base mod 16#1_0000_0000#));
         Regs.Write (ER_Dequeue_Ptr_Hi, Word32
           (DMA_Buffers.Event_Ring_Base  /  16#1_0000_0000#));

         Regs.Write (Context_Pointer_Lo, Word32
           (DMA_Buffers.DbC_Context_Base mod 16#1_0000_0000#));
         Regs.Write (Context_Pointer_Hi, Word32
           (DMA_Buffers.DbC_Context_Base  /  16#1_0000_0000#));

         Contexts.Clear_DbC_Context (DbC_Context);
         DbC_Context.DbC_Info :=
           (String_0_Address              => DMA_Buffers.Descriptor_Strings_Base,
            Manufacturer_String_Address   => DMA_Buffers.Descriptor_Strings_Base
                                             + 1 * String_Descriptor'Size / 8,
            Product_String_Address        => DMA_Buffers.Descriptor_Strings_Base
                                             + 2 * String_Descriptor'Size / 8,
            Serial_Number_String_Address  => DMA_Buffers.Descriptor_Strings_Base
                                             + 3 * String_Descriptor'Size / 8,
            String_0_Length               => Desc_Strings (String0).bLength,
            Manufacturer_String_Length    => Desc_Strings (Manufacturer).bLength,
            Product_String_Length         => Desc_Strings (Product).bLength,
            Serial_Number_String_Length   => Desc_Strings (Serial_Number).bLength,
            Reserved_Z                    => 0,
            others                        => 0);

         Regs.Read (MBS, Debug_Max_Burst_Size);
         DbC_Context.OUT_EP.EP_Type                := Contexts.Bulk_O;
         DbC_Context.OUT_EP.Max_Burst_Size         := MBS;
         DbC_Context.OUT_EP.Max_Packet_Size        := 1024;
         DbC_Context.OUT_EP.TR_Dequeue_Pointer_Lo  := Word28
           (Shift_Right (Transfer_Rings.Physical (2),  4) and 16#0fff_ffff#);
         DbC_Context.OUT_EP.TR_Dequeue_Pointer_Hi  := Word32
           (Shift_Right (Transfer_Rings.Physical (2), 32) and 16#ffff_ffff#);
         DbC_Context.OUT_EP.Dequeue_Cycle_State    := 1;
         DbC_Context.OUT_EP.Average_TRB_Length     := Max_Bulk_Size / 2;
         DbC_Context.IN_EP.EP_Type                 := Contexts.Bulk_I;
         DbC_Context.IN_EP.Max_Burst_Size          := MBS;
         DbC_Context.IN_EP.Max_Packet_Size         := 1024;
         DbC_Context.IN_EP.TR_Dequeue_Pointer_Lo   := Word28
           (Shift_Right (Transfer_Rings.Physical (3),  4) and 16#0fff_ffff#);
         DbC_Context.IN_EP.TR_Dequeue_Pointer_Hi   := Word32
           (Shift_Right (Transfer_Rings.Physical (3), 32) and 16#ffff_ffff#);
         DbC_Context.IN_EP.Dequeue_Cycle_State     := 1;
         DbC_Context.IN_EP.Average_TRB_Length      := Max_Bulk_Size / 2;

         Regs.Write (DbC_Protocol, Word16'(0));  -- Debug Target vendor defined.
         Regs.Write (Vendor_ID, Word16 (16#ffff#));
         Regs.Write (Product_ID, Word16 (16#dbc1#));
         Regs.Write (Device_Revision, Word16 (16#0001#));

         Regs.Write (DbC_Enable, Word8'(1));
         loop
            Regs.Read (DCE, DbC_Enable);
            exit when DCE = 1;
         end loop;

         if Apply_Intel_Quirk then
            Intel_Quirk.Reset_Port;
         end if;

         Running := False;
         Connected := False;
         DbC_Poll_Deadline := Time.Now;
         DbC_Stat_Deadline := Time.MS_From_Now (12_345);
      end if;
   end Reset;

   procedure Poll (Now : Boolean := False)
   is
      use type Word8;

      Temp8 : Word8;
      Timed_Out : Boolean;
   begin
      if Regs.Byte_Offset /= 0 then
         Timed_Out := Time.Timed_Out (DbC_Poll_Deadline);
         if Now or else Timed_Out then
            Regs.Read (Temp8, DbC_Enable);
            if Temp8 = 1 then
               Regs.Read (Temp8, Current_Connect_Status);
               if Temp8 = 1 then
                  -- Something is connected...
                  DbC_Poll_Deadline := Time.MS_From_Now (10);
                  if not Connected then
                     pragma Debug (Debug.Put_Line ("DbC connected."));
                     DbC_Run_Deadline := Time.MS_From_Now (333);
                     Connected := True;
                  end if;
                  Regs.Read (Temp8, DbC_Run);
                  if Temp8 = 1 then
                     -- ...configured too
                     if not Running then
                        pragma Debug (Debug.Put_Line ("DbC configured."));
                        Transfers.Start;
                        Running := True;
                     end if;
                  elsif Running then
                     pragma Debug (Debug.Put_Line
                       ("DbC still connected but deconfigured."));
                     DbC_Run_Deadline := Time.MS_From_Now (333);
                     Running := False;
                  else
                     Timed_Out := Time.Timed_Out (DbC_Run_Deadline);
                     if Timed_Out then
                        pragma Debug (Debug.Put_Line
                          ("DbC connection timed out."));
                        Reset;
                     end if;
                  end if;
               else
                  -- Nothing connected
                  DbC_Poll_Deadline := Time.MS_From_Now (333);
                  if Connected then
                     pragma Debug (Debug.Put_Line ("DbC disconnected."));
                     Connected := False;
                     Running := False;
                  end if;
               end if;
            else
               pragma Debug (Debug.Put_Line ("DbC got disabled, huh?"));
               Reset;
            end if;
            Events.Handle_Events;
            Timed_Out := Time.Timed_Out (DbC_Stat_Deadline);
            if Timed_Out then
               pragma Debug (Transfer_Info.Dump_Stats);
               DbC_Stat_Deadline := Time.MS_From_Now (12_345);
            end if;
         end if;
      end if;
   end Poll;

   procedure Receive (Buf : in out Buffer; Len : in out Natural)
   is
   begin
      Poll (Now => True);

      Transfers.Receive (Buf, Len);
   end Receive;

   procedure Send (Buf : Buffer; Len : in out Natural; Success : out Boolean)
   is
   begin
      Poll (Now => True);

      Transfers.Send
        (Buf         => Buf,
         Len         => Len,
         Start_Now   => Running,
         Success     => Success);
   end Send;

   procedure Ring_Doorbell (EP : Endpoint_Range)
   is
      use type Word8;
   begin
      Regs.Write (Doorbell_Target, Word8 (EP) - 2);
   end Ring_Doorbell;

end HW.DbC;

--  vim: set ts=8 sts=3 sw=3 et:
