--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

private package HW.DbC.Transfers is

   procedure Reset (Initial_Reset : Boolean);

   procedure Start;

   ----------------------------------------------------------------------------

   procedure Receive
     (Buf : in out Buffer;
      Len : in out Natural)
   with
      Post => Len <= Len'Old;

   procedure Send
     (Buf         : in     Buffer;
      Len         : in out Natural;
      Start_Now   : in     Boolean;
      Success     :    out Boolean)
   with
      Post => Len <= Len'Old;

end HW.DbC.Transfers;

--  vim: set ts=8 sts=3 sw=3 et:
