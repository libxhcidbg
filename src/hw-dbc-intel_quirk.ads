--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

with HW.Sub_Regs;
with HW.MMIO_Regs;
pragma Elaborate_All (HW.MMIO_Regs);

private package HW.DbC.Intel_Quirk
is

   procedure Reset_Port;

private

   type Sup_Protocol_Registers is
     (Revision_Major,
      Compatible_Port_Offset,
      Compatible_Port_Count);
   package Sup_Protocol_S_Regs is new Sub_Regs (Sup_Protocol_Registers);
   Sup_Protocol_Reg_Descs : constant Sup_Protocol_S_Regs.Array_T :=
     (Revision_Major          => (16#00#, 31, 24),
      Compatible_Port_Offset  => (16#08#,  7,  0),
      Compatible_Port_Count   => (16#08#, 15,  8));
   package Sup_Protocol_Regs
   is
      new MMIO_Regs (DbC.MMIO, 0, Sup_Protocol_S_Regs, Sup_Protocol_Reg_Descs);

   type Port_Registers is
     (Current_Connect_Status,
      Port_Reset);
   package Port_S_Regs is new Sub_Regs (Port_Registers);
   Port_Reg_Descs : constant Port_S_Regs.Array_T :=
     (Current_Connect_Status  => (16#00#, 0, 0),
      Port_Reset              => (16#00#, 4, 4));
   package Port_Regs is new MMIO_Regs (DbC.MMIO, 0, Port_S_Regs, Port_Reg_Descs);

end HW.DbC.Intel_Quirk;

--  vim: set ts=8 sts=3 sw=3 et:
