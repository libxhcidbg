--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

with System.Storage_Elements;
with Ada.Unchecked_Conversion;

with HW.Debug;

package body HW.DbC.Contexts
with SPARK_Mode => Off
is

   procedure Clear_DbC_Context (Context : out DbC_Context)
   is

      --  These constants must not be global, see [Q213-030].

      Null_Info_Context : constant DbC_Info_Context := DbC_Info_Context'
        (String_0_Address                 => 0,
         Manufacturer_String_Address      => 0,
         Product_String_Address           => 0,
         Serial_Number_String_Address     => 0,
         String_0_Length                  => 0,
         Manufacturer_String_Length       => 0,
         Product_String_Length            => 0,
         Serial_Number_String_Length      => 0,
         Reserved_Z                       => 0,
         Reserved_Z_2                     => 0,
         Reserved_Z_3                     => 0,
         Reserved_Z_4                     => 0);

      Null_Endpoint_Context : constant Endpoint_Context := Endpoint_Context'
        (EP_State              => Disabled,
         Reserved_Z            => 0,
         Mult                  => 0,
         Max_P_Streams         => 0,
         Linear_Stream_Array   => 0,
         Interval              => 0,
         Reserved_Z_2          => 0,
         Reserved_Z_3          => 0,
         Error_Count           => 0,
         EP_Type               => NA,
         Reserved_Z_4          => 0,
         Host_Initiate_Disable => 0,
         Max_Burst_Size        => 0,
         Max_Packet_Size       => 0,
         Dequeue_Cycle_State   => 0,
         Reserved_Z_5          => 0,
         TR_Dequeue_Pointer_Lo => 0,
         TR_Dequeue_Pointer_Hi => 0,
         Average_TRB_Length    => 0,
         Max_ESIT_Payload      => 0,
         reserved1             => 0,
         reserved2             => 0,
         reserved3             => 0);
   begin
      Context := DbC_Context'
        (DbC_Info => Null_Info_Context,
         OUT_EP   => Null_Endpoint_Context,
         IN_EP    => Null_Endpoint_Context);
   end Clear_DbC_Context;

   ----------------------------------------------------------------------------

   procedure Dump_Endpoint_Context (EC : in Endpoint_Context)
   is
      function EP_State_To_Word is new Ada.Unchecked_Conversion (Endpoint_State, Word3);
      function EP_Type_To_Word is new Ada.Unchecked_Conversion (Endpoint_Type, Word3);
   begin
      Debug.Put_Reg64 ("Endpoint Context", Word64 (System.Storage_Elements.To_Integer (EC'Address)));
      Debug.Put_Reg8  ("EP_State             ", Word8  (EP_State_To_Word (EC.EP_State)));
      Debug.Put_Reg8  ("RsvdZ                ", Word8  (EC.Reserved_Z));
      Debug.Put_Reg8  ("Mult                 ", Word8  (EC.Mult));
      Debug.Put_Reg8  ("Max_P_Streams        ", Word8  (EC.Max_P_Streams));
      Debug.Put_Reg8  ("Linear_Stream_Array  ", Word8  (EC.Linear_Stream_Array));
      Debug.Put_Reg8  ("Interval             ",         EC.Interval);
      Debug.Put_Reg8  ("RsvdZ                ",         EC.Reserved_Z_2);
      Debug.Put_Reg8  ("RsvdZ                ", Word8  (EC.Reserved_Z_3));
      Debug.Put_Reg8  ("Error_Count          ", Word8  (EC.Error_Count));
      Debug.Put_Reg8  ("EP_Type              ", Word8  (EP_Type_To_Word (EC.EP_Type)));
      Debug.Put_Reg8  ("RsvdZ                ", Word8  (EC.Reserved_Z_4));
      Debug.Put_Reg8  ("Host_Initiate_Disable", Word8  (EC.Host_Initiate_Disable));
      Debug.Put_Reg8  ("Max_Burst_Size       ",         EC.Max_Burst_Size);
      Debug.Put_Reg16 ("Max_Packet_Size      ",         EC.Max_Packet_Size);
      Debug.Put_Reg8  ("Dequeue_Cycle_State  ", Word8  (EC.Dequeue_Cycle_State));
      Debug.Put_Reg8  ("RsvdZ                ", Word8  (EC.Reserved_Z_5));
      Debug.Put_Reg32 ("TR_Dequeue_Pointer_Lo", Word32 (EC.TR_Dequeue_Pointer_Lo));
      Debug.Put_Reg32 ("TR_Dequeue_Pointer_Hi",         EC.TR_Dequeue_Pointer_Hi);
      Debug.Put_Reg16 ("Average_TRB_Length   ",         EC.Average_TRB_Length);
      Debug.Put_Reg16 ("Max_ESIT_Payload     ",         EC.Max_ESIT_Payload);
      Debug.Put_Reg32 ("RsvdO                ",         EC.reserved1);
      Debug.Put_Reg32 ("RsvdO                ",         EC.reserved2);
      Debug.Put_Reg32 ("RsvdO                ",         EC.reserved3);
   end Dump_Endpoint_Context;

end HW.DbC.Contexts;

--  vim: set ts=8 sts=3 sw=3 et:
