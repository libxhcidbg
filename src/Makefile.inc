xhcdbc-y += hw-dbc-contexts.adb
xhcdbc-y += hw-dbc-contexts.ads
xhcdbc-y += hw-dbc-dma_buffers.ads
xhcdbc-y += hw-dbc-events.adb
xhcdbc-y += hw-dbc-events.ads
xhcdbc-y += hw-dbc-intel_quirk.adb
xhcdbc-y += hw-dbc-intel_quirk.ads
xhcdbc-y += hw-dbc-transfer_info.adb
xhcdbc-y += hw-dbc-transfer_info.ads
xhcdbc-y += hw-dbc-transfer_rings.adb
xhcdbc-y += hw-dbc-transfer_rings.ads
xhcdbc-y += hw-dbc-transfers.adb
xhcdbc-y += hw-dbc-transfers.ads
xhcdbc-y += hw-dbc-trbs.adb
xhcdbc-y += hw-dbc-trbs.ads
xhcdbc-y += hw-dbc.adb
xhcdbc-y += hw-dbc.ads

hw-dbc-config-ads := $(subst //,/,$(call src-to-obj,,$(dir)/hw-dbc_config).ads)

$(hw-dbc-config-ads): $(dir)/hw-dbc_config.ads.template $(cnf)
	printf "    GENERATE   $(patsubst /%,%,$(subst $(obj)/,,$@))\n"
	sed -e's/<<DMA_BASE>>/$(CONFIG_XHCDBC_DMA_BASE)/' \
	    -e's/<<XHCI_BASE>>/$(CONFIG_XHCDBC_XHCI_BASE)/' \
	    -e's/<<XHCI_SIZE>>/$(CONFIG_XHCDBC_XHCI_SIZE)/' \
	    $< >$@
xhcdbc-gen-y += $(hw-dbc-config-ads)
