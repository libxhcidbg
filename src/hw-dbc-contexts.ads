--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

private package HW.DbC.Contexts
is

   Context_Size : constant := 64;

   Endpoint_Context_Alignment : constant := 32;

   ----------------------------------------------------------------------------

   type DbC_Info_Context is record
      String_0_Address             : Word64;
      Manufacturer_String_Address  : Word64;
      Product_String_Address       : Word64;
      Serial_Number_String_Address : Word64;
      String_0_Length              : Word8;
      Manufacturer_String_Length   : Word8;
      Product_String_Length        : Word8;
      Serial_Number_String_Length  : Word8;
      Reserved_Z                   : Word32;
      Reserved_Z_2                 : Word64;
      Reserved_Z_3                 : Word64;
      Reserved_Z_4                 : Word64;
   end record
   with
      Volatile,
      Size      => Context_Size * 8,
      Alignment => Context_Size;

   ----------------------------------------------------------------------------

   type Endpoint_State is (
      Disabled,
      Running,
      Halted,
      Stopped,
      Error,
      Reserved1,
      Reserved2,
      Reserved3)
   with Size => 3;
   for Endpoint_State use
     (Disabled  => 0,
      Running   => 1,
      Halted    => 2,
      Stopped   => 3,
      Error     => 4,
      Reserved1 => 5,
      Reserved2 => 6,
      Reserved3 => 7);

   type Endpoint_Type is (
      NA,
      Isoch_O,
      Bulk_O,
      Interrupt_O,
      Control,
      Isoch_I,
      Bulk_I,
      Interrupt_I)
   with Size => 3;
   for Endpoint_Type use
     (NA          => 0,
      Isoch_O     => 1,
      Bulk_O      => 2,
      Interrupt_O => 3,
      Control     => 4,
      Isoch_I     => 5,
      Bulk_I      => 6,
      Interrupt_I => 7);

   pragma Warnings (Off, "* bits of * unused");
   type Endpoint_Context is record
      EP_State              : Endpoint_State;
      Reserved_Z            : Word5;
      Mult                  : Word2;
      Max_P_Streams         : Word5;
      Linear_Stream_Array   : Bit;
      Interval              : Word8;
      Reserved_Z_2          : Word8;
      Reserved_Z_3          : Bit;
      Error_Count           : Word2;
      EP_Type               : Endpoint_Type;
      Reserved_Z_4          : Bit;
      Host_Initiate_Disable : Bit;
      Max_Burst_Size        : Word8;
      Max_Packet_Size       : Word16;
      Dequeue_Cycle_State   : Bit;
      Reserved_Z_5          : Word3;
      TR_Dequeue_Pointer_Lo : Word28;
      TR_Dequeue_Pointer_Hi : Word32;
      Average_TRB_Length    : Word16;
      Max_ESIT_Payload      : Word16;
      reserved1             : Word32;
      reserved2             : Word32;
      reserved3             : Word32;
   end record
   with
      Volatile,
      Size      => Context_Size * 8,
      Alignment => Endpoint_Context_Alignment;
   for Endpoint_Context use record
      EP_State              at 16#00# range 0 .. 2;
      Reserved_Z            at 16#00# range 3 .. 7;
      Mult                  at 16#00# range 8 .. 9;
      Max_P_Streams         at 16#00# range 10 .. 14;
      Linear_Stream_Array   at 16#00# range 15 .. 15;
      Interval              at 16#00# range 16 .. 23;
      Reserved_Z_2          at 16#00# range 24 .. 31;
      Reserved_Z_3          at 16#04# range 0 .. 0;
      Error_Count           at 16#04# range 1 .. 2;
      EP_Type               at 16#04# range 3 .. 5;
      Reserved_Z_4          at 16#04# range 6 .. 6;
      Host_Initiate_Disable at 16#04# range 7 .. 7;
      Max_Burst_Size        at 16#04# range 8 .. 15;
      Max_Packet_Size       at 16#04# range 16 .. 31;
      Dequeue_Cycle_State   at 16#08# range 0 .. 0;
      Reserved_Z_5          at 16#08# range 1 .. 3;
      TR_Dequeue_Pointer_Lo at 16#08# range 4 .. 31;
      TR_Dequeue_Pointer_Hi at 16#0c# range 0 .. 31;
      Average_TRB_Length    at 16#10# range 0 .. 15;
      Max_ESIT_Payload      at 16#10# range 16 .. 31;
      reserved1             at 16#14# range 0 .. 31;
      reserved2             at 16#18# range 0 .. 31;
      reserved3             at 16#1c# range 0 .. 31;
   end record;
   pragma Warnings (On, "* bits of * unused");

   ----------------------------------------------------------------------------

   --  See "eXtensible Host Controller Interface for Universal Serial Bus"
   --  revision 1.1, section 7.6.9, figure 120.
   type DbC_Context is record
      DbC_Info : DbC_Info_Context;
      OUT_EP   : Endpoint_Context;
      IN_EP    : Endpoint_Context;
   end record
   with
      Volatile,
      Alignment => Context_Size;
   -- Use size for alignment:
   -- It always fullfills page crossing requirements.

   for DbC_Context use record
      DbC_Info at 16#00# range 0 .. Context_Size * 8 - 1;
      OUT_EP   at 16#40# range 0 .. Context_Size * 8 - 1;
      IN_EP    at 16#80# range 0 .. Context_Size * 8 - 1;
   end record;

   -----------------------------------------------------------------------

   procedure Clear_DbC_Context (Context : out DbC_Context);

   procedure Dump_Endpoint_Context (EC : in Endpoint_Context);

end HW.DbC.Contexts;

--  vim: set ts=8 sts=3 sw=3 et:
