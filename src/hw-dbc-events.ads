--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

private package HW.DbC.Events
with
   Abstract_State => ((State with Part_Of  => HW.DbC.State),
                      (DMA   with Part_Of  => HW.DbC.DMA,
                                  External => (Async_Readers, Async_Writers)))
is

   type ERST_Entry is record
      Segment_Base : Word64;
      Segment_Size : Word32;
      Reserved_Z   : Word32;
   end record
   with
      Pack,
      Volatile,
      Alignment => 64;

   ----------------------------------------------------------------------------

   procedure Reset_Ring;

   procedure Handle_Events;

end HW.DbC.Events;

--  vim: set ts=8 sts=3 sw=3 et:
