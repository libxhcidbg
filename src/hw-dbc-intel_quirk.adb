--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

package body HW.DbC.Intel_Quirk
is

   procedure Reset_Port
   is
      use type Word8;
      Success : Boolean;
      Temp8, Op_Offset : Word8;
      Port_Count, Port_Offset : Natural;
   begin
      Cap_Regs.Read (Op_Offset, Capability_Registers_Length);
      xCap_Regs.Byte_Offset := 0;
      loop
         Find_Next_xCap (2, Success);
         exit when not Success;

         Sup_Protocol_Regs.Byte_Offset := xCap_Regs.Byte_Offset;
         Sup_Protocol_Regs.Read (Temp8, Revision_Major);
         if Temp8 = 16#03# then
            Sup_Protocol_Regs.Read (Temp8, Compatible_Port_Offset);
            if Temp8 /= 16#00# then
               Port_Offset := Natural (Temp8 - 1);

               Sup_Protocol_Regs.Read (Temp8, Compatible_Port_Count);
               Port_Count := Natural (Temp8);

               for Idx in Port_Offset .. Port_Offset + Port_Count - 1 loop
                  Port_Regs.Byte_Offset :=
                     Natural (Op_Offset) + 16#400# + Idx * 16#10#;
                  Port_Regs.Read (Temp8, Current_Connect_Status);
                  if Temp8 = 16#00# then
                     Port_Regs.Write (Port_Reset, Word8'(1));
                  end if;
               end loop;
            end if;
         end if;
      end loop;
   end Reset_Port;

end HW.DbC.Intel_Quirk;

--  vim: set ts=8 sts=3 sw=3 et:
