--
-- Copyright (C) 2016-2017 secunet Security Networks AG
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--

with HW.DbC;
with HW.Debug;

use type HW.Bit;
use type HW.Word32;

package body HW.DbC.TRBs
is

   Debug_Error_Codes : constant Boolean := False;

   function CC_To_Usb_Error (CC : Completion_Code) return Error
   is
      Err : Error;
   begin
      case CC is
      when Success                     => Err := DbC.Success;
      when Data_Buffer_Error           => Err := DbC.Communication_Error;
      when Babble_Detected_Error       => Err := DbC.Communication_Error;
      when USB_Transaction_Error       => Err := DbC.Communication_Error;
      when TRB_Error                   => Err := DbC.Driver_Error;
      when Stall_Error                 => Err := DbC.Stall_Error;
      when Short_Packet                => Err := DbC.Data_Residue;
      when Bandwidth_Error             => Err := DbC.Not_Enough_Bandwidth;
      when Command_Aborted             => Err := DbC.Timeout;
      when Secondary_Bandwidth_Error   => Err := DbC.Not_Enough_Bandwidth;
      when Split_Transaction_Error     => Err := DbC.Communication_Error;
      when others                      => Err := DbC.Unknown_Error;
         pragma Debug (Debug.Put_Reg8 ("Unknown_Error", Word8 (CC)));
      end case;
      pragma Debug
        (Debug_Error_Codes and Err /= DbC.Success and Err /= DbC.Unknown_Error,
         Debug.Put_Reg8 ("Completion code", Word8 (CC)));
      return Err;
   end CC_To_Usb_Error;

   ----------------------------------------------------------------------------

   procedure Set_Length (Data : in out T; Length : in Natural)
   is
      Status : constant Word32 := Data.Status;
   begin
      Data.Status := (Status and not 16#1_ffff#) or
        (Word32 (Length) and 16#1_ffff#);
   end Set_Length;

   procedure Get_Event_Length (Data : in T; Length : out Natural)
   is
      Status : constant Word32 := Data.Status;
   begin
      Length := Natural (Status and 16#ff_ffff#);
   end Get_Event_Length;

   ----------------------------------------------------------------------------

   procedure Get_Completion_Code (Data : in T; Code : out Completion_Code)
   is
      Status : constant Word32 := Data.Status;
   begin
      Code := Completion_Code (Shift_Right (Status, 24));
   end Get_Completion_Code;

   ----------------------------------------------------------------------------

   procedure Set_Cycle (Data : in out T; Cycle : in Bit)
   is
      Control : constant Word32 := Data.Control;
   begin
      Data.Control := (Control and not 1) or Word32 (Cycle);
   end Set_Cycle;

   procedure Get_Cycle (Data : in T; Cycle : out Bit)
   is
      Control : constant Word32 := Data.Control;
   begin
      Cycle := Bit (Control and 1);
   end Get_Cycle;

   procedure Set_Toggle_Cycle (Data : in out T)
   is
      Control : constant Word32 := Data.Control;
   begin
      Data.Control := Control or 16#00_02#;
   end Set_Toggle_Cycle;

   procedure Set_ISP (Data : in out T)
   is
      Control : constant Word32 := Data.Control;
   begin
      Data.Control := Control or 16#00_04#;
   end Set_ISP;

   procedure Set_IOC (Data : in out T)
   is
      Control : constant Word32 := Data.Control;
   begin
      Data.Control := Control or 16#00_20#;
   end Set_IOC;

   ----------------------------------------------------------------------------

   procedure Set_Type (Data : in out T; TRB_Type : in TRB_Types)
   is
      Control : constant Word32 := Data.Control;
   begin
      Data.Control := (Control and not 16#fc00#) or
        Shift_Left (Word32 (TRB_Type), 10);
   end Set_Type;

   procedure Get_Type (Data : in T; TRB_Type : out TRB_Types)
   is
      Control : constant Word32 := Data.Control;
   begin
      TRB_Type := TRB_Types (Shift_Right (Control, 10) and 63);
   end Get_Type;

   ----------------------------------------------------------------------------

   procedure Get_Endpoint_ID (Data : in T; Endpoint_ID : out Natural)
   is
      Control : constant Word32 := Data.Control;
   begin
      Endpoint_ID := Natural (Shift_Right (Control, 16) and 16#1f#);
   end Get_Endpoint_ID;

   procedure Get_Slot_ID (Data : in T; Slot_ID : out Word8)
   is
      Control : constant Word32 := Data.Control;
   begin
      Slot_ID := Word8 (Shift_Right (Control, 24));
   end Get_Slot_ID;

   ----------------------------------------------------------------------------

   procedure Set_Parameter (Data : in out T; Parameter : Word64)
   is
   begin
      Data.Parameter := Parameter;
   end Set_Parameter;

   procedure Get_Parameter (Data : in T; Parameter : out Word64)
   is
   begin
      Parameter := Data.Parameter;
   end Get_Parameter;

   ----------------------------------------------------------------------------

   procedure Clear (TR : out T; PCS : in Bit)
   is
   begin
      TR := T'
        (Parameter => 0, Status => 0, Control => Word32 ((not PCS) and 1));
   end Clear;

   procedure Clear_Ring (TR : out Transfer_Ring; PCS : in Bit)
   is
   begin
      TR := Transfer_Ring'
        (Ring_Range => T'
           (Parameter => 0, Status => 0, Control => Word32 ((not PCS) and 1)));
   end Clear_Ring;

   procedure Init_Cycle_Ring
     (Ring     :    out Transfer_Ring;
      Physical : in     Word64)
   is
   begin
      Clear_Ring (Ring, 1);

      declare
         Last : constant Ring_Range := Ring'Last;
      begin

         -- Link last T in the ring to the first.

         Set_Type (Ring (Last), Link);
         Set_Toggle_Cycle (Ring (Last));
         Ring (Last).Parameter := Physical;
      end;
   end Init_Cycle_Ring;

end HW.DbC.TRBs;

--  vim: set ts=8 sts=3 sw=3 et:
